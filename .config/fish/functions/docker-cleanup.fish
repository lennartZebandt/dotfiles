function docker-cleanup
	command docker system prune -af --volumes
	command docker image prune -af
	command docker container prune -f
end

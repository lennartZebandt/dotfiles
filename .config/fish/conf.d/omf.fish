# Path to Oh My Fish install.
set -q XDG_DATA_HOME
  and set -gx OMF_PATH "$XDG_DATA_HOME/omf"
  or set -gx OMF_PATH "$HOME/.local/share/omf"

# Load Oh My Fish configuration.
source $OMF_PATH/init.fish

# Add cargo to path
set PATH "$HOME/.cargo/bin:$PATH"

# Add golang to path
set PATH "/usr/lib/go-1.14/bin:$PATH"
set PATH "$HOME/go/bin:$PATH"

# Add local bin to path
set PATH "$HOME/.local/bin:$PATH"
set PATH "$HOME/bin:$PATH"  

# Set editor variable
set EDITOR "/usr/bin/nano"

# Apt abbrevations
abbr a "sudo apt"
abbr au "sudo apt update"
abbr auf "sudo apt upgrade"
abbr ai "sudo apt update && sudo apt install"
abbr as "apt search"
abbr ar "sudo apt remove"

# Git abbrevations
abbr gwip 'git add . && git commit -m "WIP"'
abbr gwipp 'git add . && git commit -m "WIP" && git push'
abbr push "git push -u origin HEAD"
abbr pull "git pull"
abbr delbranch "git branch -D"
abbr rebase "git pull && git rebase"
abbr rebase-abort "git rebase --abort"
abbr amend "git commit --amend"

# Other abbrevations
abbr hisgrep "history | grep"
abbr clip "xclip -sel clip <"
abbr clip-pub "xclip -sel clip < ~/.ssh/id_ed25519.pub"
abbr c "clear"
abbr w "which"
abbr ls "br -dp"
abbr du "br --sizes"
abbr home "cd && clear"
abbr sh "find ~ -name"
abbr s "sudo find / -name"
abbr eh "find ~ -type -f | fzf > $EDITOR"
abbr show-alias "less .config/fish/conf.d/omf.fish | grep --color=never -e '\(abbr\|alias\)'"
abbr reload-fish "source ~/.config/fish/conf.d/omf.fish"

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# cUrl webapp abbrevations
abbr weather "curl https://wttr.in/hambergen"
abbr myip "curl ifconfig.co"
abbr cheat "curl cheat.sh/"
abbr dict "curl dict://dict.org/d:"

function fish_greeting
	printf '\033[1;34m'
	figlet -f big J.A.R.V.I.S
	printf '\033[0m'
	neofetch 
end
